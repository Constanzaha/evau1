<%-- 
    Document   : resultado
    Created on : Apr 10, 2021, 10:16:00 PM
    Author     : const
--%>

<%@page import="Modelo.InteresSimple"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    InteresSimple is = (InteresSimple)request.getSession().getAttribute("isimple");
    
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado Interés Simple</title>
    </head>
    <body>
        <h1>Resultado Interés Simple</h1>
        <p>Su interés es de: <%=is.interessimple()%></p> <br/>
        <a href ="index.jsp">Volver a página principal</a>
    </body>
</html>

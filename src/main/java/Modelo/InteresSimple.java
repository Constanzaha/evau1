package Modelo;

public class InteresSimple {
    private double capital;
    private double interesanual;
    private double numeroaños; 
    private double interessimple;

    public InteresSimple(double capital, double interesanual, double numeroaños) {
        this.capital = capital;
        this.interesanual = interesanual;
        this.numeroaños = numeroaños;
    }

    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public double getInteresanual() {
        return interesanual;
    }

    public void setInteresanual(double interesanual) {
        this.interesanual = interesanual;
    }

    public double getNumeroaños() {
        return numeroaños;
    }


    public void setNumeroaños(double numeroaños) {
        this.numeroaños = numeroaños;
    }


    public double getInteressimple() {
        return interessimple;
    }

    public void setInteressimple(double interessimple) {
        this.interessimple = interessimple;
    }
  
    public double interessimple(){
        return this.getCapital()*(this.getInteresanual()/100)*this.getNumeroaños();
    }
    
    public double interessimple(double capital, double interesanual, double numeroaños ){  
        this.setCapital(capital);
        this.setInteresanual(interesanual);
        this.setNumeroaños(numeroaños);
        
        return interessimple();
    }  
}

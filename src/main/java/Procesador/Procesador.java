/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Procesador;

import Modelo.InteresSimple;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author const
 */
public class Procesador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String capital = request.getParameter("txtCapital");
        String interesanual = request.getParameter("txtIAnual");
        String numeroaños = request.getParameter("txtNumA");
        String error = "";
        
        if(capital.equals("")||interesanual.equals("")||numeroaños.equals("")){
            error="Complete todos los campos";
            request.getSession().setAttribute("error", error);
            request.getRequestDispatcher("errorcampos.jsp").forward(request, response);
          
        }else{
            double cap = 0;
            double ian = 0;
            double num = 0;
           
            try{
            cap = Integer.parseInt(capital);
            ian = Integer.parseInt(interesanual);
            num= Integer.parseInt(numeroaños);
            }
            catch(NumberFormatException ex){
                request.getRequestDispatcher("validacion.jsp").forward(request,response);
            }
            
            InteresSimple is = new InteresSimple (cap, ian, num); 
            is.interessimple();
            request.getSession().setAttribute("isimple", is);
            request.getRequestDispatcher("resultado.jsp").forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
